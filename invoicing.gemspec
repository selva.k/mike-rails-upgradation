$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "invoicing/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "invoicing"
  s.version     = Invoicing::VERSION
  s.authors     = ["Mike Jeremiah"]
  s.email       = ["selva.k@digiryte.com"]
  s.summary     = "Invoicing engine."
  s.description = "Invoicing engine."

  s.files = Dir["{app,config,db,lib}/**/*"] + ["LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["spec/**/*"]
  s.add_dependency "rails", "~> 4.2"
  s.add_dependency "rake", "~> 12.3.3"
  s.add_dependency "haml"
  s.add_dependency "combine_pdf"
  # s.add_dependency "jquery-rails"

  s.add_development_dependency "mysql2"
  s.add_development_dependency "rspec-rails"
  s.add_development_dependency "factory_girl_rails"

end
