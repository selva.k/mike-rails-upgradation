class CreateInvoicingInvoiceItems < ActiveRecord::Migration
  def change
    create_table :invoicing_invoice_items do |t|
      t.references :invoice
      t.datetime :invoiced_at
      t.references :author
      t.references :project
      t.references :report
      t.decimal :amount, :precision => 8, :scale => 2
      t.string :description
      t.references :valuation_firm
      t.text :invoice_address
      t.string :full_name
      t.string :email
      t.references :invoice_address
      t.timestamps
    end
    add_index :invoicing_invoice_items, :invoice_id
    add_index :invoicing_invoice_items, :author_id
    add_index :invoicing_invoice_items, :project_id
    add_index :invoicing_invoice_items, :report_id
    add_index :invoicing_invoice_items, :valuation_firm_id
  end
end
