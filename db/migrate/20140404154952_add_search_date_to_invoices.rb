class AddSearchDateToInvoices < ActiveRecord::Migration
  def change
    add_column :invoicing_invoices, :search_date, :datetime
  end
end
