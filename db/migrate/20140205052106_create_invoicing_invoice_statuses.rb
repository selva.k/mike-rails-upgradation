class CreateInvoicingInvoiceStatuses < ActiveRecord::Migration
  def change
    create_table :invoicing_invoice_statuses do |t|
      t.string :value

      t.timestamps
    end
  end
end
