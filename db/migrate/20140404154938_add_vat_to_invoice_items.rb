class AddVatToInvoiceItems < ActiveRecord::Migration
  def change
    add_column :invoicing_invoice_items, :vat, :boolean, :default => true
  end
end
