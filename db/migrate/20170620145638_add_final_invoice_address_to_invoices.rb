class AddFinalInvoiceAddressToInvoices < ActiveRecord::Migration
  def change
    add_column :invoicing_invoices, :final_invoice_address, :text
  end
end
