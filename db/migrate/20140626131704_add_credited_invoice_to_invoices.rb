class AddCreditedInvoiceToInvoices < ActiveRecord::Migration
  def change
    add_column :invoicing_invoices, :credited_invoice_number, :string
  end
end
