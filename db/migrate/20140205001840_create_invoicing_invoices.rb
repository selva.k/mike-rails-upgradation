class CreateInvoicingInvoices < ActiveRecord::Migration
  def change
    create_table :invoicing_invoices do |t|
      t.references :invoice_number
      t.datetime :invoiced_at
      t.references :author
      t.references :valuation_firm
      t.references :last_updated_by
      t.references :invoice_status

      t.timestamps
    end
    add_index :invoicing_invoices, :invoice_number_id
    add_index :invoicing_invoices, :author_id
    add_index :invoicing_invoices, :valuation_firm_id
    add_index :invoicing_invoices, :invoice_status_id
  end
end
