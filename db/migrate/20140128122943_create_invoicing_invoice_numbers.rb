class CreateInvoicingInvoiceNumbers < ActiveRecord::Migration
  def change
    create_table :invoicing_invoice_numbers do |t|
      t.string :value

      t.timestamps
    end

    add_index :invoicing_invoice_numbers, :value
  end
end
