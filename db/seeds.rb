Invoicing::InvoiceStatus.find_or_create_by!(:value => Invoicing::InvoiceStatus::STATUS[:pending_send])
Invoicing::InvoiceStatus.find_or_create_by!(:value => Invoicing::InvoiceStatus::STATUS[:ready_to_send])
Invoicing::InvoiceStatus.find_or_create_by!(:value => Invoicing::InvoiceStatus::STATUS[:on_hold])
Invoicing::InvoiceStatus.find_or_create_by!(:value => Invoicing::InvoiceStatus::STATUS[:sent_to_client])
Invoicing::InvoiceStatus.find_or_create_by!(:value => Invoicing::InvoiceStatus::STATUS[:exported])
