$(function() {
  $('.status_selector').change(function(event) {
      event.preventDefault();
      event.stopPropagation();

      var statusText = $('option:selected', this).text();
      var statusNotEditableWarning = 'ready to send';

      if ( statusText != statusNotEditableWarning ||
          ( statusText == statusNotEditableWarning && confirm('Are you sure? After this step you won\'t be able to edit the invoice.'))) {
          var invoice_id = $(this).data('invoice_id')
          var status_id = $(this).val();
          var url = '/invoicing/invoice/' + invoice_id + '/update_status?invoice_status_id=' + status_id;

          $.ajax({
            url: url,
            type: 'POST'
          });

      }
  });

  $('.add_invoice_item').click(function(event) {
      event.preventDefault();
      event.stopPropagation();

      var invoiceId = $(this).data('invoice_id');
      var url = '/invoicing/invoice_items/new/' + invoiceId
      window.location = url;
  });

});
