require "invoicing/engine"

module Invoicing
  mattr_accessor :invoiceNumberPrefix, :invoiceNumberDigits

  def self.invoiceNumberPrefix
    @@invoiceNumberPrefix || 'ENV'
  end

  def self.invoiceNumberDigits
    @@invoiceNumberDigits || 6
  end

  mattr_accessor :author_class

  def self.author_class
    @@author_class.constantize || 'User'.constantize
  end

  mattr_accessor :project_class

  def self.project_class
    @@project_class.constantize || 'Project'.constantize
  end

  mattr_accessor :report_class

  def self.report_class
    @@report_class.constantize || 'Report'.constantize
  end

  mattr_accessor :valuation_firm_class

  def self.valuation_firm_class
    @@valuation_firm_class.constantize || 'ValuationFirm'.constantize
  end

  def self.default_start
    Date.today.beginning_of_month.beginning_of_day
  end

  def self.default_end
    default_start.end_of_month.end_of_day
  end

  # def self.month_start
  #   Date.today.beginning_of_month - 1.month
  # end
  #
  # def self.month_end
  #   month_start.end_of_month
  # end

  # def self.format_defaults(the_date)
  #   Date.strptime("{ #{the_date.year}, #{the_date.month}, #{the_date.day} }", "{ %Y, %m, %d }")
  # end
end
