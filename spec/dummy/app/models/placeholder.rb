require 'active_model'

class Placeholder

  attr_accessor :name

  def name
    'placeholder name'
  end

  def id
    1
  end
end