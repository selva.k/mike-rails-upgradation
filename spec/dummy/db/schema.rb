# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170620145638) do

  create_table "invoicing_invoice_items", force: :cascade do |t|
    t.integer  "invoice_id",         limit: 4
    t.datetime "invoiced_at"
    t.integer  "author_id",          limit: 4
    t.integer  "project_id",         limit: 4
    t.integer  "report_id",          limit: 4
    t.decimal  "amount",                           precision: 8, scale: 2
    t.string   "description",        limit: 255
    t.integer  "valuation_firm_id",  limit: 4
    t.text     "invoice_address",    limit: 65535
    t.string   "full_name",          limit: 255
    t.string   "email",              limit: 255
    t.integer  "invoice_address_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "vat",                                                      default: true
  end

  add_index "invoicing_invoice_items", ["author_id"], name: "index_invoicing_invoice_items_on_author_id", using: :btree
  add_index "invoicing_invoice_items", ["invoice_id"], name: "index_invoicing_invoice_items_on_invoice_id", using: :btree
  add_index "invoicing_invoice_items", ["project_id"], name: "index_invoicing_invoice_items_on_project_id", using: :btree
  add_index "invoicing_invoice_items", ["report_id"], name: "index_invoicing_invoice_items_on_report_id", using: :btree
  add_index "invoicing_invoice_items", ["valuation_firm_id"], name: "index_invoicing_invoice_items_on_valuation_firm_id", using: :btree

  create_table "invoicing_invoice_numbers", force: :cascade do |t|
    t.string   "value",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invoicing_invoice_numbers", ["value"], name: "index_invoicing_invoice_numbers_on_value", using: :btree

  create_table "invoicing_invoice_statuses", force: :cascade do |t|
    t.string   "value",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "invoicing_invoices", force: :cascade do |t|
    t.integer  "invoice_number_id",       limit: 4
    t.datetime "invoiced_at"
    t.integer  "author_id",               limit: 4
    t.integer  "valuation_firm_id",       limit: 4
    t.integer  "last_updated_by_id",      limit: 4
    t.integer  "invoice_status_id",       limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "search_date"
    t.string   "credited_invoice_number", limit: 255
    t.text     "final_invoice_address",   limit: 65535
  end

  add_index "invoicing_invoices", ["author_id"], name: "index_invoicing_invoices_on_author_id", using: :btree
  add_index "invoicing_invoices", ["invoice_number_id"], name: "index_invoicing_invoices_on_invoice_number_id", using: :btree
  add_index "invoicing_invoices", ["invoice_status_id"], name: "index_invoicing_invoices_on_invoice_status_id", using: :btree
  add_index "invoicing_invoices", ["valuation_firm_id"], name: "index_invoicing_invoices_on_valuation_firm_id", using: :btree

end
