# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :invoicing_invoice_status, :class => Invoicing::InvoiceStatus do
    value "MyString"
  end
end
