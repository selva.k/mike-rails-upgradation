# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :invoicing_invoice_item, :class => Invoicing::InvoiceItem do
#    invoice nil
    invoiced_at "2014-02-04 21:01:43"
#    author nil
#    project nil
#    report nil
    amount "9.99"
    description "Invoice Item"
    valuation_firm_id 2
  end
end
