# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :invoicing_invoice, :class => Invoicing::Invoice do
    invoiced_at "2014-02-06 07:23:40"
    valuation_firm_id 1
  end

  factory :invoicing_invoice_with_items, parent: :invoicing_invoice, :class => Invoicing::Invoice do

    after(:create) do |invoice|
      3.times { create(:invoicing_invoice_item, invoice_id: invoice.id, valuation_firm_id: invoice.valuation_firm_id) }
    end
  end

  factory :invoicing_invoice_sent_to_client, parent: :invoicing_invoice do
    invoice_status { Invoicing::InvoiceStatus.find_by_value(Invoicing::InvoiceStatus::STATUS[:sent_to_client]) }
  end
end
