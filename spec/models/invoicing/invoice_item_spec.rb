require 'spec_helper'

module Invoicing
  describe InvoiceItem do
    describe '.readonly' do
      context 'when there is no invoice' do
        it 'should not be readonly' do
          invoiceItem = FactoryGirl.create(:invoicing_invoice_item)
          expect(invoiceItem.readonly?).to be_falsey
        end
      end

      context 'when there is an editable invoice' do
        it 'should not be readonly' do
          invoice = FactoryGirl.create(:invoicing_invoice_with_items)

          invoice.invoice_status = Invoicing::InvoiceStatus.find_by_value(Invoicing::InvoiceStatus::STATUS[:pending_send])
          invoice.save
          expect(invoice.invoice_items.first.readonly?).to be_falsey
        end
      end

      context 'when there is an invoice in sent_to_client' do
        it 'should be readonly' do
          invoice = FactoryGirl.create(:invoicing_invoice_with_items)
          invoice.stub(:client_name_and_address).and_return('client_name_and_address')

          invoice.invoice_status = Invoicing::InvoiceStatus.find_by_value(Invoicing::InvoiceStatus::STATUS[:ready_to_send])
          invoice.save

          invoice.invoice_status = Invoicing::InvoiceStatus.find_by_value(Invoicing::InvoiceStatus::STATUS[:sent_to_client])
          invoice.save

          expect(invoice.invoice_items.first.readonly?).to be_truthy
        end
      end

      context 'when there is an invoice in ready_to_send' do
        it 'should be readonly' do
          invoice = FactoryGirl.create(:invoicing_invoice_with_items)
          invoice.stub(:client_name_and_address).and_return('client_name_and_address')
          invoice.invoice_status = Invoicing::InvoiceStatus.find_by_value(Invoicing::InvoiceStatus::STATUS[:ready_to_send])
          invoice.save

          expect(invoice.invoice_items.first.readonly?).to be_truthy
        end
      end
    end

    describe '.create' do
      context 'when no invoice exists for the valuation_firm in the current month' do
        it 'should create a new invoice' do
          invoiceItem = FactoryGirl.create(:invoicing_invoice_item)
          expect(invoiceItem.invoice).not_to be_nil
        end
      end

      context 'when an invoice exists in another month' do
        it 'should create a new invoice' do
          invoice = FactoryGirl.create(:invoicing_invoice, created_at: DateTime.now.in_time_zone(Time.zone).beginning_of_month - 1.day)

          invoiceItem = FactoryGirl.create(:invoicing_invoice_item)

          invoiceItem.invoice.should_not be_nil
          invoiceItem.invoice.should_not eq invoice
        end
      end

      context 'when an invoice exists in the same month' do
        context 'and it has the status pending_send' do
          it 'should use it' do
            invoice = FactoryGirl.create(:invoicing_invoice, created_at: DateTime.now.in_time_zone(Time.zone).beginning_of_month,
                        invoice_status_id: Invoicing::InvoiceStatus::STATUS[:pending_send], valuation_firm_id: 1)

            invoiceItem = FactoryGirl.create(:invoicing_invoice_item, valuation_firm_id: 1)

            invoiceItem.invoice.should_not be_nil
            invoiceItem.invoice.should eq invoice
          end
        end

        context 'and it has another status then pending_send' do
          it 'should create a new invoice' do
            Invoice.any_instance.stub(client_name_and_address: 'client_name_and_address')
            invoice = FactoryGirl.create(:invoicing_invoice,
                                          created_at: DateTime.now.in_time_zone(Time.zone).beginning_of_month,
                                          invoice_status_id: Invoicing::InvoiceStatus.find_by_value(Invoicing::InvoiceStatus::STATUS[:ready_to_send]).id)

            invoiceItem = FactoryGirl.create(:invoicing_invoice_item)

            invoiceItem.invoice.should_not be_nil
            invoiceItem.invoice.should_not eq invoice
          end
        end
      end
    end
  end
end
