require 'spec_helper'

module Invoicing
  describe InvoiceMailer do

    describe '#send_single_invoice' do

      before :each do
        @user = double()
        @user.stub(:full_name).and_return('Lucas')
        @user.stub(:email).and_return('lucas@email.com')
        stub_const("Enviro::Application::PROJECT_FEE", 100)
        allow(File).to receive(:read).and_return("data")

        @invoice = FactoryGirl.create(:invoicing_invoice_with_items)
        @invoice.stub(:invoice_path).and_return("data")
        @invoice.stub_chain(:client_user, :full_name).and_return("Client Name")
      end

      context "a single hsbc invoice is created with a positive amount" do

        let(:email_text) { "HSBC Environmental" }

        it "should create an hsbc template for an invoice" do

          @invoice.stub(:is_hsbc?).and_return(true)
          @invoice.stub(:amount).and_return(100)
          mail =  InvoiceMailer.send_single_invoice(@user, [@invoice])
          expect(mail.body.encoded).to include email_text

          @invoice.stub(:is_hsbc?).and_return(false)
          mail =  InvoiceMailer.send_single_invoice(@user, [@invoice])
          expect(mail.body.encoded).not_to include email_text

          @invoice.stub(:is_hsbc?).and_return(true)
          @invoice.stub(:amount).and_return(-100)
          mail =  InvoiceMailer.send_single_invoice(@user, [@invoice])
          expect(mail.body.encoded).not_to include email_text
        end
      end

      context "a single lend invoice is created with a positive amount" do

        let(:email_text) { "removal from EnviroRisk Wizard system" }

        it "should create an lend template for an invoice" do

          @invoice.stub(:is_hsbc?).and_return(false)
          @invoice.stub(:amount).and_return(100)
          mail =  InvoiceMailer.send_single_invoice(@user, [@invoice])
          expect(mail.body.encoded).to include email_text

          @invoice.stub(:is_hsbc?).and_return(true)
          mail =  InvoiceMailer.send_single_invoice(@user, [@invoice])
          expect(mail.body.encoded).not_to include email_text

          @invoice.stub(:is_hsbc?).and_return(false)
          @invoice.stub(:amount).and_return(-100)
          mail =  InvoiceMailer.send_single_invoice(@user, [@invoice])
          expect(mail.body.encoded).not_to include email_text
        end
      end

      context "a single credit note is created" do

        let(:email_text) { "Please find attached our credit note for" }

        it "should create a credit note template" do

          @invoice.stub(:is_hsbc?).and_return(false)
          @invoice.stub(:amount).and_return(-100)
          mail =  InvoiceMailer.send_single_invoice(@user, [@invoice])
          expect(mail.body.encoded).to include email_text

          @invoice.stub(:amount).and_return(100)
          mail =  InvoiceMailer.send_single_invoice(@user, [@invoice])
          expect(mail.body.encoded).not_to include email_text
        end
      end

      context "a credit note and invoice are created" do

        let(:email_text) { "credit note and reissued invoice" }

        it "should create a credit note and invoice template" do

          @invoice.stub(:is_hsbc?).and_return(false)
          @invoice.stub(:amount).and_return(100)

          credit_note = FactoryGirl.create(:invoicing_invoice_with_items)
          credit_note.stub(:invoice_path).and_return("data")
          credit_note.stub_chain(:client_user, :full_name).and_return("Client Name")
          credit_note.stub(:is_hsbc?).and_return(false)
          credit_note.stub(:amount).and_return(-100)

          mail =  InvoiceMailer.send_single_invoice(@user, [@invoice, credit_note])
          expect(mail.body.encoded).to include email_text

          credit_note.stub(:amount).and_return(100)
          mail =  InvoiceMailer.send_single_invoice(@user, [@invoice])
          expect(mail.body.encoded).not_to include email_text

        end
      end

    end
  end
end

