require 'spec_helper'

module Invoicing
  describe Invoice do
    
    describe 'has_been_credited?' do
      context 'invoice has been credited' do
        it 'should be true' do
          invoice = FactoryGirl.create(:invoicing_invoice_with_items)
          expect(invoice.has_been_credited?).to be_false
          invoice.invoice_status = Invoicing::InvoiceStatus.find_by_value(Invoicing::InvoiceStatus::STATUS[:ready_to_send])
          Invoice.any_instance.stub(client_name_and_address: 'client_name_and_address')
          invoice.save
          invoice.invoice_status = Invoicing::InvoiceStatus.find_by_value(Invoicing::InvoiceStatus::STATUS[:sent_to_client])
          invoice.save
          invoice.credit(1)
          expect(invoice.has_been_credited?).to be_true
        end
      end
    end
    
    describe 'credit' do
      context "credit selected from invoice list" do
        it "should create a credit note which is the reverse of the invoice and replacement copy" do
          invoice = FactoryGirl.create(:invoicing_invoice_with_items)
          invoice.invoice_status = Invoicing::InvoiceStatus.find_by_value(Invoicing::InvoiceStatus::STATUS[:ready_to_send])
          Invoice.any_instance.stub(client_name_and_address: 'client_name_and_address')
          invoice.save
          invoice.invoice_status = Invoicing::InvoiceStatus.find_by_value(Invoicing::InvoiceStatus::STATUS[:sent_to_client])
          invoice.save
          credit_note = invoice.credit(1)
          replacement_invoice = invoice.reissue(1)
          credit_note.amount.should eq invoice.amount*-1
          credit_note.invoice_status.value.should eq Invoicing::InvoiceStatus::STATUS[:ready_to_send]
          replacement_invoice.amount.should eq invoice.amount
          replacement_invoice.invoice_status.value.should eq Invoicing::InvoiceStatus::STATUS[:ready_to_send]
        end
      end
    end

    describe 'related_invoice_for_credit_note' do
      it "should equal invoice credited" do
        invoice = FactoryGirl.create(:invoicing_invoice_with_items)
        invoice.invoice_status = Invoicing::InvoiceStatus.find_by_value(Invoicing::InvoiceStatus::STATUS[:ready_to_send])
        invoice.stub(:client_name_and_address).and_return('client_name_and_address')
        invoice.save
        invoice.invoice_status = Invoicing::InvoiceStatus.find_by_value(Invoicing::InvoiceStatus::STATUS[:sent_to_client])
        invoice.save
        credit_note = invoice.credit(1)
        expect(invoice).to eq credit_note.related_invoice_for_credit_note
        expect(invoice.related_invoice_for_credit_note).to be_nil
        expect(invoice.client_name_and_address).to eq credit_note.client_name_and_address
      end
    end

    describe 'before_save' do
      context "invoice status changes to ready to send and doesn't have invoice number" do
        it 'should generate an invoice number and set search date' do
          invoice = FactoryGirl.create(:invoicing_invoice)
          invoice.invoice_number.should be_nil
          invoice.search_date.should be_within(1).of(invoice.created_at)
          invoice.invoice_status = Invoicing::InvoiceStatus.find_by_value(Invoicing::InvoiceStatus::STATUS[:ready_to_send])
          invoice.stub(:client_name_and_address).and_return('client_name_and_address')
          invoice.save
          invoice.invoice_number.should_not be_nil
          invoice.search_date.should be_within(1).of(invoice.invoiced_at)
        end
      end

      context 'invoice status changes to ready to send and has invoice number' do
        it 'should keep the same invoice number' do
          invoice = FactoryGirl.create(:invoicing_invoice)
          invoice_number = FactoryGirl.create(:invoicing_invoice_number)
          invoice.invoice_number = invoice_number
          invoice.save
          invoice.invoice_status = Invoicing::InvoiceStatus.find_by_value(Invoicing::InvoiceStatus::STATUS[:ready_to_send])
          invoice.save
          invoice.invoice_number.should eq invoice_number
        end
      end

      context 'invoice status changes to on hold' do
        it 'should not generate an invoice number' do
          invoice = FactoryGirl.create(:invoicing_invoice)
          invoice.invoice_number.should be_nil

          invoice.invoice_status = Invoicing::InvoiceStatus.find_by_value(Invoicing::InvoiceStatus::STATUS[:pending_send])
          invoice.save
          invoice.invoice_number.should be_nil
        end
      end

      it 'should have a pending_send status by default' do
        invoice = FactoryGirl.create(:invoicing_invoice)
        invoice.invoice_status.should_not be_nil
        invoice.invoice_status.value.should eq Invoicing::InvoiceStatus::STATUS[:pending_send]
      end
    end

    describe 'amount' do
      it 'should be the sum of invoice_items' do
        invoice = FactoryGirl.create(:invoicing_invoice_with_items)
        FactoryGirl.create(:invoicing_invoice_item, amount: 15)

        invoice.amount.should eq Invoicing::InvoiceItem.where(invoice_id: invoice).sum(:amount)
      end
    end

    describe 'readonly' do
      context 'when invoice switches state to sent to client' do
        it 'should be able to' do
          invoice = FactoryGirl.create(:invoicing_invoice)
          invoice.stub(:client_name_and_address).and_return('client_name_and_address')
          invoice.invoice_status = Invoicing::InvoiceStatus.find_by_value(Invoicing::InvoiceStatus::STATUS[:ready_to_send])
          invoice.save

          invoice.invoice_status = Invoicing::InvoiceStatus.find_by_value(Invoicing::InvoiceStatus::STATUS[:sent_to_client])
          expect(invoice.save).to be_truthy
        end

        it 'should prevent further changes' do
          invoice = FactoryGirl.create(:invoicing_invoice)
          invoice.stub(:client_name_and_address).and_return('client_name_and_address')
          invoice.invoice_status = Invoicing::InvoiceStatus.find_by_value(Invoicing::InvoiceStatus::STATUS[:ready_to_send])
          invoice.save

          invoice.invoice_status = Invoicing::InvoiceStatus.find_by_value(Invoicing::InvoiceStatus::STATUS[:sent_to_client])
          invoice.save
          expect(invoice.can_edit?).to be_falsey
        end
      end
    end

    describe "show_export_button?" do

      context "sent invoices exists, after 25th of month, latest Agresso contains title and filename" do
        it "should be true" do
          allow(Invoice).to receive(:sent_invoices_exist?).and_return(true)
          allow(Time).to receive(:now).and_return("26-12-2016".to_time)
          Agresso = double("Invoicing::Agresso")
          Agresso.stub_chain("last.title") { "title" }
          Agresso.stub_chain("last.xls_filename") { "xls_filename" }
          expect(Invoice.show_export_button?).to be_true
        end
      end

      context "sent invoices do not exist, after 25th of month, latest Agresso contains title and filename" do
        it "should be true" do
          allow(Invoice).to receive(:sent_invoices_exist?).and_return(false)
          allow(Time).to receive(:now).and_return("26-12-2016".to_time)
          Agresso = double("Invoicing::Agresso")
          Agresso.stub_chain("last.title") { "title" }
          Agresso.stub_chain("last.xls_filename") { "xls_filename" }
          expect(Invoice.show_export_button?).to be_false
        end
      end

      context "sent invoices exist, before 25th of month, latest Agresso contains title and filename" do
        it "should be true" do
          allow(Invoice).to receive(:sent_invoices_exist?).and_return(true)
          allow(Time).to receive(:now).and_return("24-12-2016".to_time)
          Agresso = double("Invoicing::Agresso")
          Agresso.stub_chain("last.title") { "title" }
          Agresso.stub_chain("last.xls_filename") { "xls_filename" }
          expect(Invoice.show_export_button?).to be_false
        end
      end

      context "sent invoices exist, after 25th of month, latest Agresso does not contain title and filename" do
        it "should be true" do
          allow(Invoice).to receive(:sent_invoices_exist?).and_return(true)
          allow(Time).to receive(:now).and_return("26-12-2016".to_time)
          Agresso = double("Invoicing::Agresso")
          Agresso.stub_chain("last.title") { nil }
          Agresso.stub_chain("last.xls_filename") { nil }
          expect(Invoice.show_export_button?).to be_false
        end
      end

    end
  end
end
