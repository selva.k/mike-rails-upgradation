require 'spec_helper'

module Invoicing
  describe InvoiceNumber do
    it 'should not allow duplicate invoice numbers'   do
      invoicingNumber = FactoryGirl.create(:invoicing_invoice_number)
      FactoryGirl.build(:invoicing_invoice_number, value: invoicingNumber.value).should_not be_valid
    end

    it 'should generate consecutive invoice numbers' do
      invoicingNumber = FactoryGirl.create(:invoicing_invoice_number)
      invoicingNumber2 = FactoryGirl.create(:invoicing_invoice_number)

      invoicingNumber.value.split(Invoicing.invoiceNumberPrefix)[1].to_i.should eql(invoicingNumber2.value.split(Invoicing.invoiceNumberPrefix)[1].to_i - 1)
    end
  end

end
