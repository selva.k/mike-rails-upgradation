module Invoicing
  module ApplicationHelper
    # => "12/05/2010"
    def date_ddmmyyyy(the_date)
      the_date.strftime("%d/%m/%Y") unless the_date.nil?
    end

    def time_zone_date_and_time(the_date)
      date_ddmmyy_t(the_date.in_time_zone('London')) unless the_date.nil?
    end

    def date_ddmmyy_t(the_date)
      the_date.strftime("%d/%m/%y %H:%M") unless the_date.nil?
    end

    def quick_link(date)
      @quick_link_search_invoice = SearchInvoice.quick_link_month(date)
      link_to @quick_link_search_invoice.start_date.strftime("%B"),index_invoices_path(@quick_link_search_invoice.id)
    end

    def this_month
      Time.zone.now.strftime("%B %Y")
    end

  end
end
