module Invoicing
  class InvoiceMailer < ActionMailer::Base
    add_template_helper(Invoicing::ApplicationHelper)

    default from: "noreply@enviro.com"

    def send_single_invoice(user, invoices)
      @user = user
      credit_note_count = 0
      invoice_count = 0
      @invoice = nil

      invoices.each do |invoice|
        if invoice.credit_note?
          attachments.inline["credit-note-#{invoice.display_invoice_number}.pdf"] = File.read(invoice.invoice_path)
          credit_note_count += 1
          @invoice = invoice
        else
          attachments.inline["invoice-#{invoice.display_invoice_number}.pdf"] = File.read(invoice.invoice_path)
          invoice_count += 1
          @invoice = invoice if @invoice.nil?
        end

      end

      if credit_note_count == 0 && invoice_count > 0
        if @invoice.is_hsbc?
          email_template = :hsbc
        else
          email_template = :lend
        end
      elsif credit_note_count > 0 && invoice_count == 0
        email_template = :credit_only
      elsif credit_note_count > 0 && invoice_count > 0
        email_template = :credit_invoice
      end

      email_with_name = "#{@user.full_name} <#{@user.email}>"

      mail(:to => email_with_name, :subject => "EnviroRisk Wizard Report Invoice", :bcc => ['clair.wilkinson@wmg.com']) do |format|
        format.text { render email_template }
      end

    end

    def send_proposals_invoices
      attachments["check_ready_to_send_invoices.pdf"] = File.read(Invoice.invoice_proposals_filename)
      mail(:to => ["selva.k@digiryte.com","clair.wilkinson@wmg.com"], :subject => "All Invoices Ready to Send")
    end
  end
end
