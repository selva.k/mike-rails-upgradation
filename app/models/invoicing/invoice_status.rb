module Invoicing
  class InvoiceStatus < ActiveRecord::Base
    has_many :invoices

    STATUS = {
        :pending_send   => 'invoice proposal',
        :ready_to_send  => 'ready to send',
        :sent_to_client => 'sent to client',
        :exported       => 'exported'
    }

    def self.can_edit
        [find_by_value(Invoicing::InvoiceStatus::STATUS[:pending_send]).id]
    end

    def self.can_credit
        [find_by_value(Invoicing::InvoiceStatus::STATUS[:exported]).id,find_by_value(Invoicing::InvoiceStatus::STATUS[:sent_to_client]).id]
    end

  end


end
