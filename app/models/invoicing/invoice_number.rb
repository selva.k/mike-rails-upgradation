module Invoicing
  class InvoiceNumber < ActiveRecord::Base
    has_one :invoice
    validates :value, uniqueness: true

    before_create do |invNumber|
      # Generate the next consecutive invoice number
      prefix = Invoicing.invoiceNumberPrefix
      digits = Invoicing.invoiceNumberDigits

      lastInvoiceNumber = Invoicing::InvoiceNumber.where('value LIKE ?', "#{prefix}%").order('value desc').first

      if lastInvoiceNumber
        lastNumber = lastInvoiceNumber.value.split(prefix)[1].to_i
        invNumber.value = prefix + (lastNumber + 1).to_s.rjust(digits, '0')
      else
        invNumber.value = prefix + '1'.rjust(digits, '0')
      end
    end

  end
end
