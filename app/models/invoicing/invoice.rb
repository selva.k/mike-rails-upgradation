module Invoicing
  require 'render_anywhere'
  class Invoice < ActiveRecord::Base
    include RenderAnywhere
    include ApplicationHelper
    belongs_to :invoice_number
    has_many :invoice_items, :dependent => :destroy
    has_many :uploads_invoices, :dependent => :destroy
    belongs_to :invoice_status

    belongs_to :valuation_firm, class_name: Invoicing.valuation_firm_class.to_s
    belongs_to :author, class_name: Invoicing.author_class.to_s

    validates :valuation_firm_id, :presence => true

    before_save do
      if self.invoice_status == InvoiceStatus.find_by_value(InvoiceStatus::STATUS[:sent_to_client]) &&
          self.invoice_status_id_was != InvoiceStatus.find_by_value(InvoiceStatus::STATUS[:ready_to_send]).id
        false
      end
      # Generate invoice number only after the invoice is created, so we don't end up with orphan invoice numbers.
      if (!invoice_number && invoice_status == InvoiceStatus.find_by_value(InvoiceStatus::STATUS[:ready_to_send]))
        self.create_invoice_number!
        self.final_invoice_address = client_name_and_address
        self.invoiced_at = Time.zone.now
        self.search_date = invoiced_at
      end

      true
    end

    before_create do
      if (!invoice_status)
        self.invoice_status = InvoiceStatus.find_by_value(InvoiceStatus::STATUS[:pending_send])
        self.search_date = Time.zone.now
      end
    end

    def add_pdf_to_invoice
      uploads_invoice = UploadsInvoice.new(:invoice_id => id,:project_id => 0)
      uploads_invoice.doc = File.new(pdf_filename)
      uploads_invoice.save
    end

    def pdf_filename
      "#{Rails.root}/tmp_reports/invoice_#{id}.pdf"
    end

    def invoice_path
      File.join(Rails.root, self.uploads_invoices.last.doc.to_s.gsub(/\?.*/, ''))
    end

    def self.invoice_proposals_filename
      File.join(Rails.root.to_s,"check_ready_to_send_invoices.pdf")
    end

    def self.mark_all_ready(search_invoice, current_user)
      invoices = search(search_invoice).where("invoice_status_id = ?",InvoiceStatus.find_by_value(InvoiceStatus::STATUS[:pending_send]).id)
      invoices.each do |invoice|
        if invoice.address_complete?
          invoice.invoice_status_id = InvoiceStatus.find_by_value(InvoiceStatus::STATUS[:ready_to_send]).id
          invoice.last_updated_by_id = current_user.id
          invoice.save
          invoice.generate_invoice_pdf(invoice.pdf_filename,true)
        end
      end

    end

    def self.generate_multiple_invoices_pdf(search_invoice)
      FileUtils.rm_f(invoice_proposals_filename)
      invoices = check_invoices_search(search_invoice)

      invoice_str = ""
      for invoice_to_add in invoices
        str = ApplicationController.new.render_to_string :template => "invoicing/invoice_mailer/send_single_invoice.html.erb", :locals => {:@invoice => invoice_to_add
          },:layout => false
        invoice_str << str
      end

      @kit = PDFKit.new(invoice_str,:print_media_type => true,
                                                  :margin_top => '9mm',
                                                  :margin_left => '9mm',
                                                  :margin_right => '9mm',
                                                  :margin_bottom => '10mm',
                                                  :page_size => 'A4',
                                                  :zoom => 1.5)

      pdf = @kit.to_pdf
      file = @kit.to_file("check_ready_to_send_invoices.pdf")
      sleep 15
      Invoicing::InvoiceMailer.send_proposals_invoices.deliver
    end

    def generate_invoice_pdf(pdf_filename,add_to_invoice=false)
      invoice_pdf =  render :template => 'invoicing/invoice_mailer/send_single_invoice.html.erb', :locals => {:@invoice => self}, :layout => false
      #footer_file = "321"
      @kit = PDFKit.new(invoice_pdf,:print_media_type => true,
                                                  #:footer_html=>footer_file,
                                                  :margin_top => '9mm',
                                                  :margin_left => '9mm',
                                                  :margin_right => '9mm',
                                                  :margin_bottom => '10mm',
                                                  :page_size => 'A4',
                                                  :zoom => 1.5)

      pdf = @kit.to_pdf
      file = @kit.to_file(pdf_filename)

      add_pdf_to_invoice if add_to_invoice
    end
    handle_asynchronously :generate_invoice_pdf unless Rails.env == "test"

    def client_name_and_address
      return final_invoice_address unless final_invoice_address.blank?
      if client_address
        if id==448 || id==446
          "#{client_address.try(:user).try(:full_name)}<br/>ES (Group) Ltd<br/>#{client_address.address.gsub(/\n/, '<br/>')}".html_safe
        elsif id==898
          dtz = ValuationFirm.where(:company_name => "DTZ").first
          "#{dtz.client_address.try(:user).try(:full_name)}<br/>#{dtz.company_name}<br/>#{dtz.client_address.address.gsub(/\n/, '<br/>')}".html_safe
        elsif id==1096
          "#{client_address.try(:user).try(:full_name)}<br/>DTZ Devenham Tie Leung Limited<br/>125 Old Broad Street<br/>London<br/>EC2N 1AR".html_safe
        else
          "#{client_address.try(:user).try(:full_name)}<br/>#{valuation_firm.company_name}<br/>#{client_address.address.gsub(/\n/, '<br/>')}".html_safe
        end
      end
    end

    def client_address
      valuation_firm.client_address
    end

    def client_user
      valuation_firm.client_user
    end

    def address_complete?
      client_user && !client_address.address.blank?
    end
#
    def self.search(search_invoice)
      valuation_firm = ValuationFirm.where(company_name: search_invoice.valuation_firm).first
      invoices = where("search_date between ? AND ?",search_invoice.start_date,search_invoice.end_date).order("id DESC")
      invoices = invoices.where("valuation_firm_id = ?",valuation_firm.id) unless valuation_firm.nil?
      invoices = invoices.where("invoice_status_id = ?",search_invoice.invoice_status_id) unless search_invoice.invoice_status_id.nil?
      invoices = invoices.where("invoice_number_id = ?",search_invoice.search_invoice_number_id) unless search_invoice.ignore_in_search?
      invoices
    end

    def self.excel_search(search_invoice)
      search(search_invoice).where(invoice_status_id: InvoiceStatus.find_by_value(InvoiceStatus::STATUS[:exported]).id)
    end

    def self.valuer_search(search_invoice)
      search(search_invoice).where("invoice_status_id >= ?",InvoiceStatus.find_by_value(InvoiceStatus::STATUS[:sent_to_client]).id)
    end

    def self.check_invoices_search(search_invoice)
      unsent_invoices = [InvoiceStatus.find_by_value(InvoiceStatus::STATUS[:pending_send]).id, InvoiceStatus.find_by_value(InvoiceStatus::STATUS[:ready_to_send]).id]
      search(search_invoice).where("invoice_status_id IN (?)",unsent_invoices)
    end

    def credit_note?
      amount < 0
    end

    def title
      if credit_note?
        "CREDIT NOTE"
      else
        "SALES INVOICE"
      end
    end

    def credit(current_user_id)
      credit_note = Invoice.create!(valuation_firm_id: invoice_items.first.valuation_firm_id,
                                      author_id: current_user_id,
                                      invoice_status_id: InvoiceStatus.find_by_value(InvoiceStatus::STATUS[:pending_send]).id,
                                      last_updated_by_id: current_user_id,
                                      created_at: Time.zone.now,
                                      credited_invoice_number: invoice_number.value,
                                      final_invoice_address: final_invoice_address)


      for invoice_item in invoice_items
        new_invoice_item = InvoiceItem.create(author_id: current_user_id,
          project_id: invoice_item.project_id,
          amount: (invoice_item.amount * -1),
          description: invoice_item.description,
          valuation_firm_id: invoice_item.valuation_firm_id,
          invoice_id: credit_note.id)
      end

      credit_note.update_attributes(:invoice_status_id => InvoiceStatus.find_by_value(InvoiceStatus::STATUS[:ready_to_send]).id)
      credit_note
    end

    def has_been_credited?
      !Invoice.where("id <> ? AND credited_invoice_number = ?", id, invoice_number.try(:value)).first.nil?
    end

    def reissue(current_user_id)

      new_invoice = Invoice.create(valuation_firm_id: invoice_items.first.valuation_firm_id,
                                      author_id: current_user_id,
                                      last_updated_by_id: current_user_id, created_at: Time.zone.now)

      for invoice_item in invoice_items
        new_invoice_item = Invoicing::InvoiceItem.create(author_id: current_user_id,
          project_id: invoice_item.project_id,
          amount: invoice_item.amount,
          description: invoice_item.description,
          valuation_firm_id: invoice_item.valuation_firm_id,
          invoice_id: new_invoice.id)
      end
      new_invoice.update_attributes(:invoice_status_id => InvoiceStatus.find_by_value(InvoiceStatus::STATUS[:ready_to_send]).id)
      new_invoice
    end

    def amount
      self.invoice_items.sum(:amount)
    end

    def vat_amount
      self.invoice_items.where(:vat => true ).sum(:amount) * 0.2
    end

    def total_amount
      amount + vat_amount
    end

    def can_credit?
      !has_been_credited? && self.invoice_status_id_was && Invoicing::InvoiceStatus.can_credit.include?(invoice_status_id_was)
    end

    def can_edit?
      self.invoice_status_id_was && Invoicing::InvoiceStatus.can_edit.include?(invoice_status_id_was)
    end

    def can_add_items?
      self.can_edit?
    end

    def status
      invoice_status.nil? ? '(not defined)' : invoice_status.value
    end

    def projects_on_invoice
      Project.where("id in (?)",invoice_items.map(&:project_id))
    end

    def can_be_sent?
      invoice_status == Invoicing::InvoiceStatus.find_by_value(Invoicing::InvoiceStatus::STATUS[:ready_to_send])
    end

    def display_invoice_number
      (invoice_number.nil? ? '(none)' : invoice_number.value)
    end

    def excel_description
      # 2009-12-31T00:00:00.000
      invoiced_at.strftime("%B %Y")
    end

    def excel_invoiced_at
      # 2009-12-31T00:00:00.000
      invoiced_at.strftime("%Y-%m-%d") + "T00:00:00.000"
    end

    def excel_created_at
      created_at.strftime("%Y-%m-%d") + "T00:00:00.000"
    end

    def available_statuses
      case self.status
        when InvoiceStatus::STATUS[:pending_send]
          if address_complete?
            InvoiceStatus.where(value: [InvoiceStatus::STATUS[:ready_to_send], self.status])
          else
            InvoiceStatus.where(value: [self.status])
          end
        when InvoiceStatus::STATUS[:ready_to_send]
          InvoiceStatus.where(value: [self.status])
        when InvoiceStatus::STATUS[:sent_to_client]
          InvoiceStatus.where(value: [self.status])
        else
          InvoiceStatus.where(value: [self.status])
      end
    end

    #Invoicing::Invoice.update_all_invoice_status
    def self.update_all_invoice_status
      invoices = Invoice.where(:invoice_status_id => 1)

      for invoice in invoices
        new_date = invoice.invoice_items.first.project.project_confirmed_date.at_beginning_of_month + 10.days
        invoice.update_attributes(:invoice_status_id => 2)
        pdf_filename = "#{Rails.root}/tmp_reports/invoice_#{invoice.id}.pdf"
        invoice.generate_invoice_pdf(pdf_filename)
        uploads_invoice = UploadsInvoice.new(:invoice_id => invoice.id,:project_id => 0)
        uploads_invoice.doc = File.new(pdf_filename)
        uploads_invoice.save
        invoice.update_attributes(:invoice_status_id => 4,:invoiced_at => new_date, :search_date => new_date)
      end
    end

    def self.all_sent_invoices
      where(:invoice_status_id => InvoiceStatus.find_by_value(InvoiceStatus::STATUS[:sent_to_client]).id)
    end


    def self.generate_agresso_xls
      # invoices = Invoicing::Invoice.where(:invoice_status_id => Invoicing::InvoiceStatus.find_by_value(Invoicing::InvoiceStatus::STATUS[:exported]).id)
      # invoices.update_all(:invoice_status_id=> Invoicing::InvoiceStatus.find_by_value(Invoicing::InvoiceStatus::STATUS[:sent_to_client]).id)
      agresso = Agresso.create
      @invoices = Invoice.all_sent_invoices

      include Rails.application.routes.url_helpers # brings ActionDispatch::Routing::UrlFor
      include ActionView::Helpers::TagHelper

      av = ActionView::Base.new(Rails.root.join('app', 'views'))
      av.assign({:invoices => @invoices})

      filename = File.join(Rails.root, "attachments","agresso",agresso.id.to_s + Agresso.filename(@invoices.last.invoiced_at))
      f = File.new(filename, 'w')
      f.puts(av.render(:template => "invoices/agresso"))
      f.close

      uploads_agresso = UploadsAgresso.new(:agresso_id=>agresso.id,:project_id => 0)
      uploads_agresso.doc = File.new(filename)
      uploads_agresso.save

      pdf = CombinePDF.new
      @invoices.each do |invoice|
        pdf << CombinePDF.new(invoice.invoice_path)
      end
      pdf_filename = "#{Rails.root}/tmp_reports/#{Agresso.pdf_filename(@invoices.last.invoiced_at)}"
      pdf.save pdf_filename

      uploads_agresso_all = UploadsAgressoAll.new(:agresso_id=>agresso.id,:project_id => 0)
      uploads_agresso_all.doc = File.new(pdf_filename)
      uploads_agresso_all.save


      @invoices.update_all(:invoice_status_id=> InvoiceStatus.find_by_value(InvoiceStatus::STATUS[:exported]).id)
      new_title = "#{@invoices.last.invoiced_at.strftime("%B")} #{@invoices.last.invoiced_at.strftime("%Y")}"
      agresso.update_attributes(:title => new_title,:xls_filename => filename)
    end

    def self.show_export_button?
      sent_invoices_exist? && after_25th_of_month? && latest_agresso_populated?
    end

    def self.sent_invoices_exist?
      !Invoice.all_sent_invoices.empty?
    end

    def self.after_25th_of_month?
      Time.now.day > 25
    end

    def self.latest_agresso_populated?
      !Agresso.last.title.nil? && !Agresso.last.xls_filename.nil?
    end

    def work_order
      "EED11281-#{valuation_firm.valuer_ref}"
    end

    def self.send_single_to_client(invoices)
      invoice_status = Invoicing::InvoiceStatus.find_by_value(Invoicing::InvoiceStatus::STATUS[:ready_to_send])

      begin
        invoices.each do |invoice|
          logger.debug { "lll Updating status" }
          invoice.update_attributes(:invoice_status_id => Invoicing::InvoiceStatus.find_by_value(Invoicing::InvoiceStatus::STATUS[:sent_to_client]).id)
        end
        Invoicing::InvoiceMailer.delay.send_single_invoice(invoices.first.client_user, invoices)
        return true
      rescue
        return false
      end

    end

    # user = User.find_by_username("mikej")
    # invoice = Invoicing::Invoice.first
    # Invoicing::InvoiceMailer.send_single_invoice(user,invoice).deliver
    def self.send_single(user, invoices)
      Invoicing::InvoiceMailer.delay.send_single_invoice(user,invoices)
    end

    def self.send_all(user=nil)
      Invoice.all_ready_to_send_grouped_by_valuation_firm.each do |valuation_firm_id, invoices|
        if user.nil?
          Invoice.send_single_to_client(invoices)
        else
          Invoice.send_single(user,invoices)
        end
        sleep 2
      end
    end

    def self.all_ready_to_send
      where(:invoice_status_id => InvoiceStatus.find_by_value(InvoiceStatus::STATUS[:ready_to_send]).id)
    end

    def self.all_ready_to_send_grouped_by_valuation_firm
      all_ready_to_send.group_by {|invoice| invoice.valuation_firm_id}
    end

    def is_hsbc?
      !invoice_items.joins(:project).where("projects.project_type = ?", "hsbc").empty?
    end

    def multiple_pages_flash_message
      if multiple_pages
        "The invoice will now contain multiple pages."
      else
        "The invoice will now contain a single page."
      end
    end

    def multiple_pages_link
      if multiple_pages
        "2"
      else
        "1"
      end
    end

    def page_option
      if multiple_pages
        "1of2"
      else
        "1of1"
      end
    end

    def related_invoice_for_credit_note
      if credit_note?
        Invoice.where(:invoice_number_id => InvoiceNumber.find_by_value(credited_invoice_number).id).first
      end
    end

    def regenerate_file
      self.uploads_invoices.each do |uploads_invoice|
        uploads_invoice.doc =nil
        uploads_invoice.save
      end
      self.uploads_invoices.destroy_all
      self.generate_invoice_pdf(pdf_filename,true)
    end
  end
end
