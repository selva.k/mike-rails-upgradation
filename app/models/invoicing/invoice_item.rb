module Invoicing
  class InvoiceItem < ActiveRecord::Base
    belongs_to :invoice

    belongs_to :project, class_name: Invoicing.project_class.to_s
    belongs_to :report, class_name: Invoicing.report_class.to_s
    belongs_to :author, class_name: Invoicing.author_class.to_s
    belongs_to :valuation_firm, class_name: Invoicing.valuation_firm_class.to_s

    validates :valuation_firm_id, :presence => true

    before_create do
      #beginningOfMonth = DateTime.parse(it.created_at.to_s).beginning_of_month
      currentMoment    = self.created_at ||= Time.zone.now
      beginningOfMonth = self.created_at.nil? ? DateTime.now.in_time_zone(Time.zone).beginning_of_month : DateTime.parse(self.created_at.to_s).beginning_of_month
      #currentMoment    = self.created_at ||= Time.zone.now

      if self.invoice_id.nil?
        invoices = Invoicing::Invoice.where(invoice_status_id: InvoiceStatus.find_by_value(InvoiceStatus::STATUS[:pending_send]).id).
                      where(valuation_firm_id: self.valuation_firm_id).
                      where('created_at BETWEEN ? AND ?', beginningOfMonth, currentMoment).
                      order(:created_at)

        if invoices.empty?
          self.create_invoice!(valuation_firm_id: self.valuation_firm_id, author_id: self.author_id,
                               invoice_status_id: InvoiceStatus.find_by_value(InvoiceStatus::STATUS[:pending_send]).id,
                               last_updated_by_id: self.author_id, created_at: beginningOfMonth, search_date: beginningOfMonth)
        else
          self.invoice = invoices.first
        end
      end
    end

    def readonly?
      (!self.invoice_id.nil? && !self.invoice.can_edit?) ||
          (!self.invoice_id.nil? && self.invoice.invoice_status_id_was == InvoiceStatus.find_by_value(InvoiceStatus::STATUS[:ready_to_send]).id)
    end

    def invoice_details
      unless project_id.blank?
        "#{project.project_name} #{project.waterman_ref} #{author.full_name} #{project.your_ref} #{project.invoice_created}"
      else
        project.description
      end
    end

    def other_proposals
      Invoice.where(
        'id <> ? and invoice_status_id = ?',
        invoice_id,
        InvoiceStatus.find_by_value(InvoiceStatus::STATUS[:pending_send]).id
      )
    end
  end
end
