# module Invoicing
#   class ApplicationController < ::ApplicationController
#   end
# end

class Invoicing::ApplicationController < ApplicationController
  layout 'layouts/admin'
  load_and_authorize_resource unless Rails.env == "test"
end
