require_dependency "invoicing/application_controller"


module Invoicing
  class InvoicesController < ApplicationController
  load_and_authorize_resource unless Rails.env == "test"
  #after_filter :save_html, :only => :index

     def save_html
      File.open('/tmp/makehtml.xls', 'w') do |f|
        f.write response.body
      end
    end

    def check_projects
      @start_date = params[:start_date]
      @start_date ||= "01/#{Time.zone.now.month}/#{Time.zone.now.year}"
      @end_date = params[:end_date]
      @end_date ||= "#{Time.zone.now.end_of_month.day}/#{Time.zone.now.month}/#{Time.zone.now.year}"
      @projects = Project.issue_date_search(@start_date,@end_date)

      respond_to do |format|
        format.html
        format.xls do
          headers["Content-Disposition"] = "attachment; filename=\"check_project_list.xls\""
        end
      end

    end

    def agresso
      @agressos=Agresso.order("id DESC")
    end

    def credit
      invoice = Invoicing::Invoice.find(params[:invoice_id])
      # Refactor mtj todo
      # Credit note
      @invoice = invoice.credit(current_user.id)
      @invoice.generate_invoice_pdf(@invoice.pdf_filename,true)

      # Invoice
      @invoice = invoice.reissue(current_user.id)

      redirect_to index_invoices_path, notice: "A credit note for this invoice has been created."
    end

    def mark_ready
      search_invoice = SearchInvoice.find(params[:search_invoice_id])
      Invoice.mark_all_ready(search_invoice, current_user)
      redirect_to index_invoices_path, notice: "Invoice proposals are being marked ready to send."
    end

    def check_invoices
      search_invoice = SearchInvoice.find(params[:search_invoice_id])
      search_invoice.update_attributes(:invoice_status_id => InvoiceStatus.find_by_value(InvoiceStatus::STATUS[:pending_send]).id)
      Invoice.delay.generate_multiple_invoices_pdf(search_invoice)
      search_invoice.update_attributes(:invoice_status_id => InvoiceStatus.find_by_value(InvoiceStatus::STATUS[:ready_to_send]).id)
      Invoice.delay.generate_multiple_invoices_pdf(search_invoice)
      redirect_to index_invoices_path, notice: "Invoice proposals have been sent."
    end

    def index
      if params[:reset].present?
        @search_invoice=SearchInvoice.create
      else
        if params[:search_invoice].present?
          @search_invoice=SearchInvoice.create(search_invoice_params)
        else
          if params[:id].present?
            @search_invoice=SearchInvoice.find(params[:id].to_i)
          elsif SearchInvoice.exists?(session[:search_id].to_i)
            @search_invoice=SearchInvoice.find(session[:search_id].to_i)
          else
            @search_invoice=SearchInvoice.create
          end
        end
      end
      session[:search_id] = @search_invoice.id
      respond_to do |format|
        format.html do
          if is_consultant?
            @invoices = Invoice.search(@search_invoice)
          elsif is_valuer? && is_super_user?
            if current_user.valuation_firm.company_name && @search_invoice.update_attributes(:valuation_firm => current_user.valuation_firm.company_name)
              @invoices = Invoice.valuer_search(@search_invoice)
            else
              @invoices = []
            end
          else
            @invoices = []
          end
        end
        format.xls do
          headers["Content-Disposition"] = "attachment; filename=\"invoice_list.xls\""
          @invoices = Invoice.excel_search(@search_invoice)
        end
      end
    end

    def update_status
      @invoice = Invoicing::Invoice.find(params[:invoice_id])
      invoice_status = Invoicing::InvoiceStatus.find(params[:invoice_status_id])

      if !@invoice.available_statuses.include?(invoice_status)
        flash[:error] = 'Invalid invoice status'
      elsif @invoice.invoice_status_id != invoice_status.id
        @invoice.invoice_status = invoice_status
        @invoice.last_updated_by_id = current_user.id

        if invoice_status.id == InvoiceStatus.find_by_value(InvoiceStatus::STATUS[:ready_to_send]).id
          @invoice.generate_invoice_pdf(@invoice.pdf_filename,true)
        end

        begin
          @invoice.save
        rescue
          flash[:error] = 'There was an error updating the invoice status'
        end
      end

      redirect_to :index_invoices
    end

    def preview
      @invoice = Invoicing::Invoice.find(params[:invoice_id])
      authorize! :preview, @invoice
      #render :file => 'invoicing/invoice_mailer/send_single_invoice.html.erb', :layout => false

      if @invoice.can_edit?
        # Remove temp file if it exists
        File.delete(@invoice.pdf_filename) if File.exist?(@invoice.pdf_filename)

        @invoice.generate_invoice_pdf(@invoice.pdf_filename)

        while (!FileReady.new(@invoice.pdf_filename).all_ready?)
          sleep(0.5)
        end
        sleep(2)
        send_file @invoice.pdf_filename,:type => 'application/pdf', :disposition => 'inline'
      else
        send_file @invoice.invoice_path,:type => 'application/pdf', :disposition => 'inline'
      end
    end

    def send_single
      invoice = Invoicing::Invoice.find(params[:invoice_id])
      Invoice.send_single(current_user,[invoice])
      redirect_to index_invoices_path, notice: "A copy of the invoice has been sent to #{current_user.email}."
    end

    def send_single_to_client
      invoice = Invoicing::Invoice.find(params[:invoice_id])
      if Invoice.send_single_to_client([invoice])
        redirect_to index_invoices_path, notice: "The invoice has been sent to the client."
      else
        redirect_to index_invoices_path, error: "Failed sending this invoice."
      end
    end

    def send_all
      if params[:user].present? && params[:user]=="current"
        Invoice.delay.send_all(current_user)
      else
        Invoice.delay.send_all(nil)
      end
      redirect_to index_invoices_path, notice: "All invoices are being sent."
    end

    private

    def search_invoice_params
      params.require(:search_invoice).permit(:valuation_firm, :start_date, :end_date, :invoice_status_id, :invoice_number)
    end

    def review_send_to_client
      @invoices = Invoice.all_ready_to_send
    end

    def toggle_pages
      invoice = Invoicing::Invoice.find(params[:id])
      if invoice.update_attributes(:multiple_pages => !invoice.multiple_pages)
        redirect_to index_invoices_path, notice: invoice.multiple_pages_flash_message
      else
        redirect_to index_invoices_path, notice: "The change failed to save."
      end
    end

    def export_invoices
      Invoice.delay.generate_agresso_xls
      redirect_to agresso_path, :notice => "Invoices are being exported."
    end

    def move_invoice_items
      invoice = Invoicing::Invoice.find(params[:invoice_id])
      invoice_item = Invoicing::InvoiceItem.find(params[:invoice_status_id])
      if invoice_item.update_attributes(invoice_id: invoice.id)
        flash_message = 'The invoice item has been moved.'
      else
        flash_message = 'The invoice item failed to move.'
      end
      redirect_to index_invoices_path, notice: flash_message
    end

  end

end
