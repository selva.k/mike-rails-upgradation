require_dependency "invoicing/application_controller"

module Invoicing
  class InvoiceItemsController < ApplicationController
    def new
      # Expected key should be invoice_id vlad
      if params.has_key? 'id'
        invoice_id=params[:id]
      elsif params.has_key? 'invoice_id'
        invoice_id=params[:invoice_id]
      end
      unless invoice_id.nil?
        @invoice = Invoicing::Invoice.find(invoice_id)
        @invoiceItem = Invoicing::InvoiceItem.new(:invoice_id => @invoice.id, :valuation_firm_id => @invoice.valuation_firm_id)
      else
        @invoiceItem = Invoicing::InvoiceItem.new
      end
    end

    def show
      @invoiceItem = Invoicing::InvoiceItem.find(params[:id])
    end

    def edit
      @invoiceItem = Invoicing::InvoiceItem.find(params[:id])
    end

    def update
      @invoiceItem = Invoicing::InvoiceItem.find(params[:id])

      if @invoiceItem.update_attributes(invoice_item_params)
        redirect_to index_invoices_path, notice: 'Invoice item was successfully updated.'
      else
        render action: "edit"
      end
    end

    def create
      @invoiceItem = Invoicing::InvoiceItem.new(invoice_item_params)

      if @invoiceItem.save
        redirect_to index_invoices_path, notice: 'Invoice Item was successfully created.'
      else
        render action: "new"
      end
    end

    def destroy
      @invoiceItem = Invoicing::InvoiceItem.find(params[:id])
      if !@invoiceItem.readonly?
        @invoiceItem.destroy
      end

      redirect_to index_invoices_path
    end

    private

    def invoice_item_params
      params.require(:invoice_item).permit(:author_id, :amount, :description,
        :vat, :invoice_id, :project_id, :valuation_firm_id)
    end

  end

end

