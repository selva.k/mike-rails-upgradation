Invoicing::Engine.routes.draw do

  post 'invoice/move_invoice_items/:invoice_id/:invoice_status_id' => 'invoices#move_invoice_items', :as => :move_invoice_items
  post 'invoice/export_invoices' => 'invoices#export_invoices', :as => :export_invoices
  post 'invoice/:id/toggle_pages' => 'invoices#toggle_pages', :as => :invoices_toggle_pages
  post 'invoice/:search_invoice_id/mark_ready' => 'invoices#mark_ready', :as => :invoices_mark_ready
  get 'invoices/review_send_to_client' => 'invoices#review_send_to_client', :as => :invoices_review_send_to_client
  get 'invoices/check_projects' => 'invoices#check_projects', :as => :invoices_check_projects
  match 'invoices/index/(:id)' => 'invoices#index', :as => :index_invoices, :via => [:get,:patch]
  match 'invoices/generate_excel_from_previous_month', :as => :generate_excel_from_previous_month, :via => [:get]
  get 'invoices/agresso' => 'invoices#agresso', :as => :agresso
  post 'invoice/:invoice_id/update_status' => 'invoices#update_status', :as => :update_status_invoice
  get 'invoice/:invoice_id/send_single' => 'invoices#send_single', :as => :send_single_invoice
  get 'invoice/:invoice_id/send_single_to_client' => 'invoices#send_single_to_client', :as => :send_single_to_client_invoice
  match 'invoice/:user/send_all' => 'invoices#send_all', :as => :send_all_invoices, :via => [:post]
  get 'invoice/:invoice_id/preview' => 'invoices#preview', :as => :preview_invoice
  get 'invoice/:invoice_id/credit' => 'invoices#credit', :as => :credit_invoice

  post 'invoice/:search_invoice_id/check_invoices' => 'invoices#check_invoices', :as => :check_invoices
  get 'invoice/invoice_pdf' => 'invoices#invoice_pdf', :as => :invoice_pdf
  get 'invoice_items/edit/:invoice_id' => 'invoice_items#edit', :as => :edit_invoice_item_for_invoice, :via => [:get]

  resources :invoice_items
  get 'invoice_items/new/:invoice_id' => 'invoice_items#new', :as => :new_invoice_item_for_invoice

  root :to => 'invoices#index'
end
